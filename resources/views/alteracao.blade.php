@extends('layout.main')

@section('titulo', 'Alteração')

@section('janela-titulo', 'Alteração')

@section('conteudo')
    <main>
        <form class="busca" action="/alteracaoBuscar" method="POST">
            @csrf
            <div>
                <label class="label-busca">CPF:</label>
                <input class="input-busca" minlength="11" maxlength="11" type="text" minlength="11" maxlength="11" required name="cpf">

                <input class="btn azul" id="submit-busca" type="submit" value="Buscar">
            </div>
        </form>
        <hr>
        <div class="forms">
        
            <form  action="/atualizar" method="post">
                @csrf
                
                @if(isset($dados))

                    @foreach ($dados as $dado)
                        
                        <input type="text" name="id" value="{{$dado->id}}" style="display: none"> 

                        <label>Estado</label>
                        <select name="estado" required>

                            @foreach ($estados as $id => $estado) 

                                @if ($id == $dado->estado)
                                    <option value="{{$id}}" selected> {{ $estado->estado }} </option>
                                @endif
                                
                                <option value="{{$id}}"> {{ $estado->estado }} </option>

                            @endforeach
                            
                        </select>

                        <br>

                        <label>Nome:</label>
                        <input type="text" name="nome" id="nome" maxlength="100" required value="{{$dado->nome}}">

                        <br>

                        <label>CPF:</label>
                        <input type="text" name="cpf" id="nome" minlength="11" maxlength="11" required value="{{$dado->cpf}}">

                        <br>

                        <label>Cidade:</label>
                        <input type="text" name="cidade" id="nome" maxlength="50" required value="{{$dado->cidade}}">

                        <br>

                    @endforeach

                @elseif(empty($dados))

                    <label>Estado</label>
                    <select disabled required> <option value=""></option></select>

                    <br>

                    <label>Nome:</label>
                    <input type="text" id="nome" value="" disabled required>
                    
                    <br>

                    <label>CPF:</label>
                    <input type="text" id="cpf" value="" disabled required>
                    
                    <br>

                    <label>Cidade:</label>
                    <input type="text" id="cidade" value="" disabled required>

                @endif

                <div class="foot-form">
                    <input class="btn vermelho"type="button" value="Voltar" onclick=" window.location.href = '/'; ">
                    <input class="btn verde" type="submit" id="submit" onmouseover="verificaCampos()" type="submit" value="Salvar">
                </div>

            </form>

        </div>
    </main>
@endsection