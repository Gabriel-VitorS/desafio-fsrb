@extends('layout.main')

@section('titulo', 'Consulta')

@section('janela-titulo', 'Consultar')

@section('conteudo')
<main>
    <form class="busca" action="/consultaBuscar" method="POST">
        @csrf
        <div>
            <label class="label-busca">CPF:</label>
            <input class="input-busca"type="text" minlength="11" required maxlength="11" name="cpf">

            <input class="btn azul" type="submit" value="Buscar">
        </div>
    </form>
    <hr>

    <div class="forms">
        <form>

            @if(isset($dados))

                @foreach ($dados as $dado)
                <input type="text" name="id" value="{{$dado->id}}"  style="display: none"> <!-- id do usuario -->

                <label>Estado</label>
                <select disabled>

                    @foreach ($estados as $id => $estado)

                        @if ($id == $dado->estado)
                            <option value=""> {{ $estado->estado }} </option>
                        @endif

                    @endforeach
                </select>

                <br>

                <label>Nome:</label>
                <input type="text" name="nome" id="nome" value="{{$dado->nome}}" disabled>

                <br>

                <label>CPF:</label>
                <input type="number" name="cpf" value="{{$dado->cpf}}" disabled>

                <br>

                <label>Cidade:</label>
                <input type="text" name="cidade" value="{{$dado->cidade}}" disabled>

                @endforeach

            @elseif(empty($dados))

                <label>Estado</label>
                <select name="estado" disabled>    
                </select>

                <br>

                <label>Nome:</label>
                <input type="text" disabled>

                <br>

                <label>CPF:</label>
                <input type="number" disabled>

                <br>

                <label>Cidade:</label>
                <input type="text" disabled>
            @endif
            <div class="foot-form">
                <input class="btn vermelho"type="button" value="Voltar" onclick=" window.location.href = '/'; ">
            </div>
        </form>
    </div>
</main>
@endsection