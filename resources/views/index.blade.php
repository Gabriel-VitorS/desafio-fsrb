@extends('layout.main')

@section('titulo', 'Cadastramento')

@section('janela-titulo', 'Cadastramento')

@section('conteudo')
    <div id="candidato">Gabriel Vitor da Silva</div>
    <main id="cadastro">
        <a class="btn-cadastramento" href="/inclusao">Incluir</a>
        <a class="btn-cadastramento" href="/alteracao">Alterar</a>
        <a class="btn-cadastramento" href="/exclusao">Excluir</a>
        <a class="btn-cadastramento" href="/consulta">Consultar</a>
        <a class="btn-cadastramento" href="/listagem">Listagem</a>
    </main>
@endsection

@section('rodape')
    <div id="sair">
        <p onclick="sair()">Sair</p>
    </div>  
@endsection