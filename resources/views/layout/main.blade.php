<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="/img/download.png">
    <title>@yield('titulo')</title>
    <!-- NORMALIZE CSS -->
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <!-- FOLHA DE ESTILO-->
    <link rel="stylesheet" href="css/estilo.css">
</head>
<body>
    <header>
        <div id="logo">
            <a href="/">
                <img src="/img/logo_fsbr.png" alt="logo" height="50">
            </a>
        </div>

        <div id="titulo">
            <p>@yield('janela-titulo')</p>
        </div>

        <div id="fechar">
            <button onclick="sair()">X</button>
        </div>
    </header>
    @yield('conteudo')
    <footer>
    @yield('rodape')
    </footer>

    <script src="js/script.js"></script>
</body>
</html>