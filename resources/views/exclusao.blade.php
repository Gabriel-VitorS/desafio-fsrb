@extends('layout.main')

@section('titulo', 'Exclusão')

@section('conteudo')
    <main>
        <form class="busca" action="/exclusaoBuscar" method="POST">
        @csrf
            <div>
                <label class="label-busca">CPF:</label>
                <input class="input-busca"type="text" minlength="11" maxlength="11" required name="cpf">

                <input class="btn azul" type="submit" value="Buscar">
            </div>
        </form>
        <hr>

        <div class="forms" >
            <form action="/excluir" method="POST">
                @csrf

                @if(isset($dados))
                    @foreach ($dados as $dado)
                    <input type="text" name="id"  value="{{$dado->id}}"  style="display: none"> <!-- id do usuario -->

                    <label>Estado</label>
                    <select name="" id="" disabled required>

                        @foreach ($estados as $id => $estado)
                            @if ($id == $dado->estado)
                                <option value="{{$id}}"> {{ $estado->estado }} </option>
                            @endif

                        @endforeach

                    </select>

                    <br>

                    <label>Nome:</label>
                    <input type="text" name="nome" id="nome" value="{{$dado->nome}}" disabled>
                    
                    <br>

                    <label>CPF:</label>
                    <input type="text" name="cpf" value="{{$dado->cpf}}" disabled>
                    
                    <br>

                    <label>Cidade:</label>
                    <input type="text" name="cidade" value="{{$dado->cidade}}" disabled >

                    @endforeach
                    
                @elseif(empty($dados))

                    <label>Estado</label>
                    <select disabled required> <option value=""></option></select>

                    <br>

                    <label>Nome:</label>
                    <input type="text" id="nome" value="" disabled required>
                    
                    <br>

                    <label>CPF:</label>
                    <input type="text" id="cpf" value="" disabled required>
                    
                    <br>

                    <label>Cidade:</label>
                    <input type="text" id="cidade" value="" disabled required>
                @endif

            <div class="foot-form">
                <input class="btn vermelho"type="button" value="Voltar" onclick=" window.location.href = '/'; ">
                <input class="btn verde" type="submit" id="submit" onmouseover="verificaCampos()" onclick="verificaExclusao()" value="Excluir">
            </div>

            </form>
        </div>

    </main>
@endsection