@extends('layout.main')

@section('titulo', 'Inclusão')

@section('janela-titulo', 'Inclusão')

@section('conteudo')
<main>
    <div class="forms">
        <form action="/incluir" method="post">
            @csrf

            <label>Estado</label>
            <select name="estado" id="estado" required>
                <option value="">Escolha o Estado...</option>

                @foreach ($estados as $id => $estado)
                    <option value="{{$id}}"> {{$estado->estado}} </option>
                @endforeach
                
            </select>

            <br>

            <label>Nome:</label>
            <input type="text" name="nome" id="nome" maxlength="100" required placeholder="Digite o nome...">
        
            <br>
        
            <label>CPF:</label>
            <input type="text" name="cpf" id="cpf" minlength="11" maxlength="11" required placeholder="Digite o CPF..." >
        
            <br>
        
            <label>Cidade:</label>
            <input type="text" name="cidade" id="cidade" maxlength="50" required placeholder="Digite a Cidade">
        
            <br>

            <div class="foot-form">
                <input class="btn vermelho"type="button" value="Voltar" onclick=" window.location.href = '/'; ">
                <input class="btn verde" type="submit" id="submit" type="submit" value="Salvar">
            </div>
        </form>
    </div>
</main>
@endsection