@extends('layout.main')

@section('titulo', 'Listagem')

@section('janela-titulo', 'Listagem')

@section('conteudo')
    <main>
        <form class="busca" action="/listagemBuscar" method="POST">
            @csrf
            <div>
                <label class="label-busca">Nome:</label>
                <input class="input-busca" type="text" name="nome">
                <label class="label-busca">CPF:</label>
                <input class="input-busca" type="text" minlength="11" maxlength="11" name="cpf">
    
                <input class="btn azul" type="submit" value="Buscar">
            </div>
        </form>
        
        <div class="forms">
            <table>
                <thead>
                    <th id="th-id">ID</th>
                    <th id="th-nome">Nome</th>
                    <th id="th-cpf">CPF</th>
                    <th id="th-cidade">Cidade</th>
                </thead>
                <tbody>

                    @if(isset($dados))
                        @foreach ($dados as $dado)
                            <tr>
                                <td>{{$dado->id}}</td>
                                <td>{{$dado->nome}}</td>
                                <td>{{$dado->cpf}}</td>
                                <td>{{$dado->cidade}}</td>
                            </tr>
                        @endforeach

                    @elseif(empty($dados))
                        <tr>
                        </tr>
                    @endif
                    
                </tbody>
            </table>  
        </div>
        <div class="forms qtd">
            @isset($qtd)
             <p id="qtd">Qtd: {{$qtd ?? ''}} </p>
            @endisset

        </div>
        
        <div class="form qtd">
            <div class="foot-form">

                <input class="btn azul" type="button" onclick=" document.getElementsByTagName('tbody')[0].innerHTML = ''; document.getElementById('qtd').innerHTML = ''" value="Limpar">
                <input class="btn vermelho"type="button" value="Voltar" onclick=" window.location.href = '/'; "> 

            </div>
        </div>
    </main>    
@endsection