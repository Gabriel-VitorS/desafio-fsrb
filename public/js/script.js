function verificaExclusao() {

    if (window.confirm('Deseja apagar o usuario?')) {
        console.log('apagou')
    } else {
        document.getElementById('id').value = -1
    }
}

function sair() {
    if (window.confirm('Dejesa sair da aplicação?')) {
        window.location.href = 'https://www.google.com/'
    }
}

function verificaCampos() {
    let nome = document.getElementById('nome')
    let estado = document.getElementById('estado')
    let cidade = document.getElementById('cidade')

    if (nome.value == '' || estado.value == '' || cidade.value == '') {
        document.getElementById("submit").disabled = true;
        document.getElementById("submit").style.cursor = 'not-allowed'
    }
}