## Desafio FSBR

- [✅] Criar um sistema contendo a inclusão, alteração, exclusão, consulta e listagem dos registros da tabela Cadastramento.
- [✅] O campo Estado será carregado através de um endpoint que consta nos dados gerais no final deste documento.
- [✅] Deverá existir uma tela principal, contendo os botões de acesso as funcionalidades, conforme mockup existentes.

## OBS:
O arquivo ".env" foi retirado do .gitignore portanto, o mesmo já está configurado

<div align="center">
<span ><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></span>


<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>
</div>

