<?php

use App\Http\Controllers\AtualizarController;
use App\Http\Controllers\BuscasController;
use App\Http\Controllers\ExcluirController;
use App\Http\Controllers\IncluirController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Rotas Get

Route::get('/', [IndexController::class, 'index']);

Route::get('/inclusao', [IndexController::class, 'inclusao']);

Route::get('/consulta', [IndexController::class, 'consulta']);

Route::get('/listagem', [IndexController::class, 'listagem']);

Route::get('/alteracao', [IndexController::class, 'alteracao']);

Route::get('/exclusao', [IndexController::class, 'exclusao']);

//Rotas Post

Route::post('/atualizar', [AtualizarController::class, 'atualizar']);

Route::post('/incluir', [IncluirController::class, 'incluir']);

Route::post('/excluir', [ExcluirController::class, 'excluir']);


// Rotas para o controller de buscas

Route::post('alteracaoBuscar', [BuscasController::class, 'alteracaoBuscar']); 

Route::post('consultaBuscar', [BuscasController::class, 'consultaBuscar']);

Route::post('exclusaoBuscar', [BuscasController::class, 'exclusaoBuscar']);

Route::post('listagemBuscar', [BuscasController::class, 'listagemBuscar']);