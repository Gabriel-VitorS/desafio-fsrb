<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cadastramento;
class IncluirController extends Controller
{
    public function incluir(Request $request){

        if($request->nome == '' || $request->cpf == '' || $request->cidade == '' || $request->estado == ''){
            echo ("<SCRIPT>
                        window.alert('Existe campos vazios! Preencha todos') 
                        history.go(-1) 
                    </SCRIPT>");
        }else {
            $incluir = new Cadastramento();

            $incluir->nome = $request->nome;
            $incluir->cpf = $request->cpf;
            $incluir->cidade = $request->cidade;
            $incluir->estado = $request->estado;

            try{ 
                $incluir->save();
                echo ("<SCRIPT>
                            window.alert('Cadastro Realizado com Sucesso.') 
                            window.location.href='/'
                        </SCRIPT>");
                //return redirect('/');
            }catch(\Exception $e){
                echo ("<SCRIPT>
                            window.alert('Houve algum problema na inclusão, tentar novamente.')
                            history.go(-1) 
                        </SCRIPT>");
            }
        }
    }
}
