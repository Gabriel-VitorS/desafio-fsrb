<?php

namespace App\Http\Controllers;

use App\Models\Estado;


class IndexController extends Controller
{
    public function index(){
        return view('index');
    }

    public function inclusao(){
        $estados = Estado::all(); 

        return view('inclusao', ['estados' => $estados]);
    }

    public function alteracao(){

        return view('alteracao');
    }

    public function exclusao(){

        return view('exclusao');
    }

    public function consulta(){

        return view('consulta');
    }

    public function listagem(){

        return view('listagem',);
    }

}
