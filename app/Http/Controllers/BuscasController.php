<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estado;
use App\Models\Cadastramento;

class BuscasController extends Controller
{
    public function alteracaoBuscar(Request $request){
        $estados = Estado::all();
        $dados = Cadastramento::where('cpf', $request->cpf)->get();
        if($dados->isEmpty()){
            echo ("
                    <SCRIPT>
                            window.alert('Dados não localizado, tente novamente.') 
                            history.go(-1) 
                    </SCRIPT>");
        }
        return view('/alteracao', ['estados' => $estados], ['dados' => $dados]);
    }

    public function consultaBuscar(Request $request){
        $estados = Estado::all();
        $dados = Cadastramento::where('cpf', $request->cpf)->get();
        if($dados->isEmpty()){
            echo ("
                    <SCRIPT>
                            window.alert('Dados não localizado, tente novamente.') 
                            history.go(-1) 
                    </SCRIPT>");
        }
        return view('/consulta', ['estados' => $estados], ['dados' => $dados]);
    }

    public function exclusaoBuscar(Request $request){
        $estados = Estado::all();
        $dados = Cadastramento::where('cpf', $request->cpf)->get();
        if($dados->isEmpty()){
            echo ("
                    <SCRIPT>
                            window.alert('Dados não localizado, tente novamente.') 
                            history.go(-1) 
                    </SCRIPT>");
        }
        return view('/exclusao', ['estados' => $estados], ['dados' => $dados]);
    }

    public function listagemBuscar(Request $request){


        if($request->cpf == '' and $request->nome == ''){
            echo ("<SCRIPT>
                        window.alert('Campos de busca vazios') 
                        history.go(-1) 
                     </SCRIPT>");
            die;
        }

        $dados =[];
        $qtd = [];
        if($request->cpf !== null ){

            $dados = Cadastramento::where('cpf', $request->cpf)->get();
            $qtd = $dados->count();
            
        }elseif($request->nome !== null){

            $dados = Cadastramento::where('nome', 'LIKE', '%' . $request->nome . '%')->get();
            $qtd = $dados->count();

        }elseif($request->nome !== null and $request->cpf !== null){

            $dados = Cadastramento::where('nome', 'LIKE', '%' . $request->nome . '%')->where('cpf', $request->cpf)->get();
            $qtd = $dados->count();

        }
        if(empty($dados) or $dados->isEmpty()){
            echo ("
                    <SCRIPT>
                        window.alert('Dados não localizado, tente novamente.')  
                    </SCRIPT>");
        }
        


        return view('/listagem', ['dados' => $dados], ['qtd' => $qtd]);
    }
}
