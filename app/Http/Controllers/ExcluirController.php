<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cadastramento;
class ExcluirController extends Controller
{
    public function excluir(Request $request){

        if($request->id == -1){
            echo ("<SCRIPT>
                    history.go(-1) 
                 </SCRIPT>");

        }else{
            $remover = Cadastramento::find($request->id);
            try{ 
                $remover->delete();
                echo ("<SCRIPT>
                            window.alert('Cadastro Excluído com Sucesso') 
                            window.location.href='/'
                        </SCRIPT>");
            }catch(\Exception $e){
                echo ("<SCRIPT>
                            window.alert('Houve algum problema na exclusão, tentar novamente.')
                            history.go(-1) 
                        </SCRIPT>");
            }
        }
    }
}
