<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cadastramento;
use FFI\Exception;

class AtualizarController extends Controller
{
    public function atualizar(Request $request){

        
        if($request->id == ''){ 
            echo ("<SCRIPT>
                    window.alert('Campos Vazios. Faça a busca primeiro') 
                    history.go(-1) 
                </SCRIPT>");
        }else {
            $atualizar = Cadastramento::find($request->id);
        
            $atualizar->nome = $request->nome;
            $atualizar->cpf = $request->cpf;
            $atualizar->cidade = $request->cidade;
            $atualizar->estado = $request->estado;
    
            try{ 
                $atualizar->save();
                echo ("<SCRIPT>
                            window.alert('Cadastro Atualizado com Sucesso.') 
                            window.location.href='/'
                        </SCRIPT>");
            }catch(\Exception $e){
                echo ("<SCRIPT>
                            window.alert('Houve algum problema na alteração, tentar novamente')
                            history.go(-1) 
                        </SCRIPT>");
            }
        }
    }
}
