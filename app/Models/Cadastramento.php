<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cadastramento extends Model
{
    use HasFactory;

    protected $table = 'cadastramento';

    public $timestamps = false;
}
